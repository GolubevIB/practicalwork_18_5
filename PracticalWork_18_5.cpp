// PracticalWork_18_5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <vector>

class Stack
{
public:
    Stack(int number)
    {
        Max = 1000;
        index = 0;
        if (number <= Max)
        {
            arr = new int[number];
            Max = number;

         }
        else std::cout << "Переполнение стека!";
    }

    ~Stack()
    {
        delete[] arr;
    };

    bool Push(int n)
    {
        if (index == Max)
            return false;
        else
        {
            arr[index] = n;
            index++;
            return true;
        }

    }

    int Pop()
    {
        if (index < 0)
            return 0;
        else
        {
            return arr[--index];
        }
    }

    bool IsEmpty()
    {
        if (index <= 0)
            return true;
        else
            return false;
    }

    bool IsFull()
    {
        if (index == Max)
            return true;
        else
            return false;
    }

private:
    int* arr;
    int Max;
    int index;
};

int main() 
{
    Stack st(25);
   
    for (int i(0); !st.IsFull(); i++)
    {
        st.Push(i + 1);
    }

    for (int i(0); !st.IsEmpty(); i++)
    {
        std::cout << st.Pop() << "\n";
    }

    return 0;
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
